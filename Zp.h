#ifndef GUARD_Zn_H
#define GUARD_Zn_H 1
#include <iostream>
#include <math.h>     
#include <boost/multiprecision/cpp_int.hpp>
using namespace boost::multiprecision;
using namespace std;
class Zn;

Zn operator+(Zn x, Zn y);
Zn operator-(Zn x, Zn y);
Zn operator*(Zn x, Zn y);
Zn operator/(Zn x, Zn y);
Zn operator^(Zn x,cpp_int n);
bool operator==(Zn x, Zn y);
bool operator!=(Zn x, Zn y);
ostream& operator<<(ostream& dest, Zn x); 
istream& operator>>(istream& sorg, Zn& x);


class Zn{
	private:
		cpp_int v;
		cpp_int p;
	public:

		Zn(const cpp_int& v,const cpp_int& p):p(p) 
			{set_v(v);}

		void set_v(cpp_int v) 
			{
			if(v<0)
				{
				cpp_int t = -1*v;
				t /= p;
				v += t*p + p; 
				}

			this->v = v%p;
			}

		cpp_int get_v() const
			{return v;}
	
		cpp_int get_p() const
			{return p;}

		Zn reciproco() const
			{
			cpp_int a = this->v;
			cpp_int b = this->p;
			cpp_int x = 0;
			cpp_int lastx = 1;
			cpp_int y = 1;
			cpp_int lasty = 0;
			while(b != 0)
				{
				cpp_int temp = b;
				cpp_int quotient = a/b;
				b = a%b;
				a = temp;
			 
				temp = x;
				x = lastx - quotient*x;
			 
				lastx = temp;
			 
				temp = y;
				y = lasty - quotient*y;
			 
				lasty = temp;
				}
			if(Zn(lastx,p)*Zn(v,p) != Zn(1,p))
				throw "reciproco non esiste";


			return Zn(lastx,p);
/*
			if(v!=0)
				{
				// dal piccolo teorema di Fermat:
				// se a non divide p 				
				// a^(p-1) = 1 (mod p) => a*a^(p-2) = 1 mod p
				// da cui a^(p-2) e' reciproco di a in modulo p.
 				if(p>2)
					return (*this)^(p-2);

				if(p==2)
					return Zn(1,p);

				throw "";

				}

			throw "reciproco non trovato";
*/			}
};

Zn operator+(Zn x, Zn y)
	{
	if(x.get_p() != y.get_p())
		throw "operazioni in modulo differente non gestite";

	cpp_int p = x.get_p();
	cpp_int a = x.get_v();
	cpp_int b = y.get_v();

	return Zn(a+b,p);
	}

Zn operator-(Zn x, Zn y)
	{
	if(x.get_p() != y.get_p())
		throw "operazioni in modulo differente non gestite";

	cpp_int p = x.get_p();
	cpp_int a = x.get_v();
	cpp_int b = y.get_v();

	return Zn(a-b,p);
	}

Zn operator*(Zn x, Zn y)
	{
	if(x.get_p() != y.get_p())
		throw "operazioni in modulo differente non gestite";

	cpp_int p = x.get_p();
	cpp_int a = x.get_v();
	cpp_int b = y.get_v();

	return Zn(a*b,p);
	}
Zn operator*(cpp_int x, Zn y)
	{return Zn(x,y.get_p())*y;}

Zn operator*(Zn x,cpp_int y)
	{return y*x;}

Zn operator/(Zn x, Zn y)
	{return x*(y.reciproco());}

Zn operator/(cpp_int x, Zn y)
	{return Zn(x,y.get_p())/y;}

Zn operator/(Zn x,cpp_int y)
	{return x/Zn(y,x.get_p());}

bool operator==(Zn x, Zn y)
	{
	if(x.get_p()!=y.get_p())
		throw "operazioni in modulo differente non gestite";

	return(x.get_v()==y.get_v());
	}

bool operator==(cpp_int x, Zn y)
	{return Zn(x,y.get_p())== y;}

bool operator==(Zn x,cpp_int y)
	{return y==x;}

bool operator!=(Zn x, Zn y)	
	{return !(x==y);}

ostream& operator<<(ostream& dest, Zn x)
	{
	dest << x.get_v() << " mod( " << x.get_p() << " )";
	return dest;
	} 

istream& operator>>(istream& sorg, Zn& x)
	{
	cpp_int a;
	cpp_int p;
	char c;
	sorg >> a;

	sorg >> ws;
	sorg >> c;
	if(c!= 'm')		
		{
		cout << "m";
		sorg.setstate(ios::failbit);
		return sorg;			
		}
	sorg >> c;
	if(c!= 'o')		
		{
		cout << "m";
		sorg.setstate(ios::failbit);
		return sorg;			
		}
	sorg >> c;
	if(c!= 'd')		
		{
		cout << "m";
		sorg.setstate(ios::failbit);
		return sorg;			
		}
	sorg >> c;
	if(c!= '(')		
		{
		cout << "m";
		sorg.setstate(ios::failbit);
		return sorg;			
		}
	sorg >> p;
	sorg >> c;
	if(c!= ')')		
		{
		sorg.setstate(ios::failbit);
		return sorg;			
		}

	x = Zn(a,p);
	return sorg;
	}

Zn operator^(Zn x,cpp_int n) 
	{
	if (n == 0)
		return Zn(1,x.get_p());

	if(n < 0)
		{
		x = 1/x;
		n *= -1;		
		}

	Zn result = Zn(1,x.get_p());
	while(n != 0)	
		{
		if(n%2 == 1)
			{
			result = result * x;
			--n;
			}
		x = x*x;
		n/=2;
		}

	return result;

	}
	/******************************************/
	// restituisce la radice quadrata discreta se n e' un residuo quadratico,
	// restituisce '0' altrimenti o se n e' nullo.
	// in questo modo si può discriminare se un valore non è residuo quadratico 
	// o se n e' nullo testando l'input  
	Zn radice_quadrata_discreta(Zn y/*n*/)
		{

		cpp_int p = y.get_p();

		//caso residuo quadratico
		//se p = 3 (mod 4) allora esiste direttamente la soluzione x=y^((p-1)/4)
		if(p%4 == 3)
			{
			//se è un non-residuo quadratico termina restituendo '0'.
			Zn u = y^((p-1)/2);
			if( u == Zn(-1,p)) 
				return Zn(0,p);

			// se e' un residuo quadratico calcola e 
			// restituisce il risultato corretto
			Zn res = y;
			res = res^((p+1)/4);  
			return res;
			}

		// se p = 1 (mod 4)..
		// scompone 'p-1' in 'm*(2^s)'
		// (s e' un esponente di 2. Presupponendo
		// che non sia mai maggiore di 'MAX_LONG', 
		// lo memorizza in un 'long' e non in un 'cpp_int')
		long s = 0;
		cpp_int m = p-1;
		while ((m%2 ) == 0) 
			{
			m /= 2;
			++s;
			}

		//cerca un non-residuo quadratico
		Zn z(1,p);
		while( (z^((p-1)/2)) != Zn(-1,p) )
			z = z + Zn(1,p);

		Zn a = y^m;
		Zn x = y^((m+1)/2);
		Zn b = z^m;
		
		for(long i=s-1; i>0; --i) 
			{
			cpp_int n = boost::multiprecision::pow(cpp_int(2),i-1);
			Zn t = a^n;
			if( (t) == (Zn(-1,p)) )
				{
				a = a*(b^2);
				x = x*b;				
				}

			b = b^2;
			}

		if((x^2) == y)
			return x;
		else
			{
			return Zn(0,p);
			}
	}


#endif
