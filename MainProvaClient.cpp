#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <sys/timeb.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>   //getopt()
#include <errno.h>
#include <stdlib.h>   // atoi()
#include <sys/types.h>
#include <dirent.h>
#include <arpa/inet.h> // inet_ntoa()
#include <signal.h>
#include <errno.h>
#include <netinet/tcp.h>
#include "ECCW.h" 

// dichiara la funzione di gestione della comunicazione TCP.
int client_side(int);

// dichiara la funzione di gestione delle opzioni.
void option_manager(int argc, char** argv, int& port);

// funzione di gestione di SIGINT
void sigint_handler(int signum);

// dichiara il socket che verra' usato per la comunicazione col server
// come globale per poterlo chiudere con 'sigint_handler' 
int sckt_fd;

int main(int argc, char *argv[])
{
	// dichiara la porta del server (per poterla modificare con le opzioni se necessario),
	// le informazioni del server e il socket necessari per la connettersi al server. 
	int port = 25003;
	struct 	sockaddr_in server;
	int  length = sizeof(server);
	int socket_fd;   
	
	// gestisce le opzioni inserite a linea di comando.
	option_manager(argc,argv,port);

	// inizializza e apre il socket TCP con indirizzamento IPv4.
	sckt_fd = socket(AF_INET,SOCK_STREAM,0);            

	// gestisce l'arrivo di SIGINT per la chiusura del 'sckt_fd'.
	signal(SIGINT, sigint_handler);

	// inizializza le informazioni del server da raggiungere.
	memset((void *)&server,0,sizeof(server));                      
	server.sin_family      = AF_INET;
	server.sin_addr.s_addr = INADDR_ANY;
	server.sin_port        = htons(port);
	printf("Socket port #%d\n",ntohs(server.sin_port));

	// tenta la connessione al server e termina il programma in caso di errore.
	socket_fd = connect(sckt_fd,(struct sockaddr *)&server,(socklen_t ) length);
	if(socket_fd < 0)
		{
		printf("error %d\n",errno);
		exit(errno);
		}
	printf("Connected.\n");

	// gestisce la connessione con il server
	client_side(sckt_fd);    

	// inizia la fase di chiusura, stampando a video un messaggio di servizio 
	// e chiudendo il socket
	printf("Closing connection with server %s, port %u\n", inet_ntoa(server.sin_addr), ntohs(server.sin_port));
	close(sckt_fd);

	return 0; 
}


// *************** client_side() ********************
// gestisce il ciclo di invio e ricezione di messaggi con il server.
int client_side(int sock)
	{
	// dichiara le strighe che conterranno i messaggi da inviare e ricevere
	// e la lunghezza di quest'ultimo  
	char  client_msg[10000];
	char  answ[10000];
	int n;
	ECCW ecc;
	ecc.imposta_chiavi();
	char ecc_msg[10000];



	// "svuota" 'client_msg' per problemi di lunghezza effettiva del messaggio 
	memset((void *)&client_msg,0,sizeof(client_msg));
	
	// imposta il socket per "non bufferizzare" e spedire immediatamente appena
	// il messaggio e' pronto.
	int flag = 1;
	setsockopt(sock, IPPROTO_TCP, TCP_NODELAY, (char *) &flag, sizeof(int));
	/**************************************************/

	strcpy(ecc_msg,ecc.scambio_DH_invia_chiave_pubblica().c_str());
	strcat(ecc_msg,"\n");
	write(sock,ecc_msg,strlen(ecc_msg));

	n = read(sock,&answ,sizeof(answ));
	if(n <= 0)
		{
		printf("connection closed by Server\n");
		return 0;		
		}

	ecc.scambio_DH_ricevi_chiave_pubblica(string(answ));
	
	// legge il messaggio dell'utente
	printf("Message to send:\n");  
	scanf("%s",client_msg);

	/*************************************************/	

	// accoda al messaggio un terminatore di riga 
	strcat(client_msg,"\n");
	strcpy(ecc_msg,ecc.cifra_elgamal(string(client_msg)).c_str());

	write(sock,ecc_msg,strlen(ecc_msg));
	printf("Message sent\n");  

	// ciclo principale: finche' non viene immessa la stringa "quit"
	while(strcmp(ecc_msg,"quit") != 0)
		{

		// "svuota" 'client_msg' per problemi di lunghezza effettiva del messaggio 
		memset((void *)&client_msg,0,sizeof(client_msg));

		// aspetta la risposta del server
		printf("Waiting for answer...\n");  
		n = read(sock,&answ,sizeof(answ));
		
		// se non riceve niente, la connessione e' stata chiusa dal server
		if(n <= 0)
			{
			printf("connection closed by Server\n");
			break;		
			}

		// stampa a video il messaggio ricevuto		
		strncpy(answ,answ,n);
		strcpy(ecc_msg, ecc.decifra_elgamal(string(answ)).c_str());

		printf("Received message from server: \n%s", ecc_msg);  

		// legge il nuovo messaggio da inviare
		printf("Message to send:\n");  
		scanf("%s",client_msg);	
	
		// invia il messaggio letto al server
		strcat(client_msg,"\n");
		strcpy(ecc_msg, ecc.cifra_elgamal(string(client_msg)).c_str());
		write(sock,ecc_msg,strlen(ecc_msg));
		printf("Message Sent\n");
		  
		}

	return 0;
	}

// ****************option_manager***********
// gestisce le opzioni da linea di comnado.
void option_manager(int argc, char* argv[], int& port)
	{
	
	char opt; 
	while ( (opt = getopt(argc, argv, "hs:p:")) != -1)
		{
		switch (opt)
			{
			//case 'h':  
				//usage();   
				//return 0;
			//case 's':   
				//strcpy(servername,optarg); 
				//break;
			case 'p':   
				port = atoi(optarg);      
				break;
			case '?':   
				printf("Option -%c not supported\n",optopt); 
				exit(1);
			}
		}
}
// ****************sigint_handler***********
// gestisce SIGINT chiudendo i socket.
void sigint_handler(int signum)
	{
	// chiude il socket e stampa un messaggio opportuno. 
	printf("\nSIGNIT captured, sockets closed\n");
	close(sckt_fd);
	exit(SIGINT);
	}

