ECC - Elliptic Curve Cryptography
Progetto per il Corso di Crittografia, Universita' degli Studi di Parma.

Classe per la codifica e decodifica di messaggi basato su curve ellittiche 
crittografiche.

Questa classe implementa lo scambio di chiavi Diffie-Hellman e il protocollo 
Elgamal per Curve Ellittiche Crittografiche.