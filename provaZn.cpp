#include <iostream>
#include <sstream>
#include <string>
#include "Zp.h"
#include "PuntoW.h"
#include "ECCW.h"
using namespace std;

int main(int argc, char* argv[]){
	
	unsigned p;

	if(argc >= 2)
		stringstream(argv[1]) >> p;
	else
		p = 101;

	for(unsigned i=0; i<p; ++i)
		try
			{
			cout << Zn(i,p) << " * " << Zn(i,p).reciproco() << " = " << Zn(i,p)*Zn(i,p).reciproco() << endl;
			}
		catch(...)
			{}
	return 0;

}



