#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <sys/timeb.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>   //getopt()
#include <errno.h>
#include <stdlib.h>   // atoi()
#include <sys/types.h>
#include <dirent.h>
#include <arpa/inet.h> // inet_ntoa()
#include <signal.h>
#include <errno.h>
#include "ECCW.h" 

// dichiara gli identificatori dei socket come globali per poterli
// usare nelle chiamate di funzione di gestione di SIGINT. 
int listen_fd;      
int connect_fd;   

// dichiara la funzione di gestione delle opzioni.
void option_manager(int argc, char** argv, int& port);

// funzione di gestione di SIGINT
void sigint_handler(int signum);

// dichiara la funzione di gestione della comunicazione TCP.
int appl_prot(int);


/***************************************************/
/****************MAIN*******************************/
/***************************************************/
/***************************************************/
int main(int argc, char *argv[])
{
	// dichiara la struttura sockaddr (usata per riferirsi al client) 
	// con relativa lunghezza della struttura 
	// e la porta di ascolto del server.
	struct 	sockaddr_in server,client;
	int  length = sizeof(client);         
	int  port = 25003;  // Porta di ascolto  

	
	// gestisce le opzioni fornite da linea di comando.
	option_manager(argc,argv,port);	

	// crea un socket per l'ascolto delle connessioni 
	// TCP (SOCK_STREAM) tramite IPv4 (AF_INET)
	// (gestendo eventuali errori terminando il programma)
	// e attiva la gestione del SIGINT per la chiusura del socket.
	listen_fd = socket(AF_INET,SOCK_STREAM,0);                       
	if(listen_fd<0)
		{
		printf("error socket listen %d\n",errno);
		exit(errno);
		}
	signal(SIGINT, sigint_handler);

	// imposta la struttura 'server' affinche' il processo possa accettare  
	// connessioni tramite IPv4 (AF_INET) da broadcast (INADDR_ANY) 
	// sulla porta 'port'.
	memset((void *)&server,0,sizeof(server));               
	server.sin_family      = AF_INET;
	server.sin_addr.s_addr = INADDR_ANY;
	server.sin_port        = htons(port);

	// imposta il socket in modo da riusare la stessa porta  
	// in caso di terminazione del processo senza chiusura
	// del socket, gestendo eventuali errori. 
	int reuse=1;
	if (setsockopt(listen_fd, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(reuse)) == -1) 
		{
		printf("setsockopt error %d\n",errno);
		exit(errno);
		}

	// associa il socket (e quindi la porta) al processo, gestendo 
	// eventuali errori.
	if(	bind(listen_fd,(struct sockaddr *)&server, sizeof(server)) < 0)    
		{
		printf("socket bind error %d\n",errno);
		exit(errno);
		}

	// mette il processo in ascolto sulla porta associata, gestendo
	// gestendo eventuali errori.
	printf("Socket port #%d\n",ntohs(server.sin_port));
	if(	listen(listen_fd,2) < 0)      
		{
		printf("errore socket listen%d\n",errno);
		exit(errno);
		}

	// ciclo principale: il processo aspetta l'arrivo di una connessione, la serve, 
	// la chiude e ricomincia.
	while(true)
		{
		// aspetta una connessione per poi "spostarla" sull'altro socket 'connect_fd'.		
		printf("Listening..\n");
		connect_fd = accept(listen_fd,(struct sockaddr *)&client,(socklen_t *) &length);  

		// stampa le informazioni del client appena connesso e gestisce il servizio tramite la funzione 'appl_prot'.
		printf("Serving connection from client %s, port %u\n",inet_ntoa(client.sin_addr),ntohs(client.sin_port));
		appl_prot(connect_fd);    

		// stampa le informazioni del client (che ha terminato la connessione) e chiude il socket associato.
		printf("Closing connection with client %s, port %u\n",inet_ntoa(client.sin_addr),ntohs(client.sin_port));
		close(connect_fd);
		}
	
	//codice morto:
	//chiude il socket e termina il processo. 
	close(listen_fd);
	return 0;
}


/***************************************************/
// funzione di gestione della connessione con il client
int appl_prot(int sock)
	{
	// dichiara le variabili che conterranno i messaggi 
	// da inviare e ricevere col client, e la loro lunghezza. 
	char  client_msg[10000];
	char  answ[10000];
	int n;
	ECCW ecc;
	char ecc_msg[10000];

	// inizializza la memoria occupata da client_msg in modo 
	// da non avere valori casuali.
	memset((void *)&client_msg, 0, sizeof(client_msg));

	n = read(sock,&client_msg,sizeof(client_msg));
	strcpy(ecc_msg,ecc.scambio_DH_ricevi_chiave_pubblica(string(client_msg)).c_str());
	write(sock,ecc_msg,strlen(ecc_msg));
	// ciclo: il processo aspetta i messaggi del client e
	// li rispedisce al mittente in coda alla stringa 'Answer: '
	// il ciclo termina se si riceve la string a 'quit' o se 
	// il client esegue una 'close'.

	do 
		{
		printf("Waiting...\n");
		// aspetta e riceve un messaggio da parte del client 
		n = read(sock,&client_msg,sizeof(client_msg));
		// se e' arrivato un messaggio, mette il terminatore 
		// di stringa alla sua fine, se e' terminata la connessione
		// (n==0) termina la gestione dei messaggi e il ciclo.
		if(n > 0)
			client_msg[n] = '\0';
		else
			{
			printf("Connection closed by client\n");
			break;
			}
		strcpy(ecc_msg,ecc.decifra_elgamal(string(client_msg)).c_str());

		// stampa il messaggio ricevuto e le informazioni del mittente 
		printf("Received message from client:\n%s\n", ecc_msg);  

		// crea la risposta nel formato "answer: client_msg"

		printf("Message to send:\n");  
		scanf("%s",client_msg);	
	
		// invia il messaggio letto al server
		strcat(client_msg,"\n");
		strcpy(ecc_msg,ecc.cifra_elgamal(string(client_msg)).c_str());
		write(sock,ecc_msg,strlen(ecc_msg));
		printf("APPL-PROT: Sent answer to the client \n");  
		}
	while(strcmp(ecc_msg,"quit") != 0);

	return 0;
	}

// *****************************************************
// funzione di gestione delle opzioni di linea di comando.
void option_manager(int argc, char** argv, int& port)
	{
	char opt; 
	while ( (opt = getopt(argc, argv, "hs:p:")) != -1)
		{
		switch (opt)
			{
			// '-p n' setta la porta del server a 'n'
			case 'p':   
				port=atoi(optarg);      
				break;
			// se non e' una opzione contemplata, stampa un messaggio 
			// di servizio e termina
			case '?':   
				printf("Option -%c not supported\n", optopt); 
				exit(1);
			}
		}
	}

// *****************************************************
// funzione di gestione di SIGINT.
void sigint_handler(int signum)
	{
	//chiude i socket e stampa un messaggio di servizio.
	printf("\nSIGNIT captured, sockets closed\n");
	close(listen_fd);
	close(connect_fd);
	exit(SIGINT);
	}

