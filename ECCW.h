#ifndef GUARD_ECC_H
#define GUARD_ECC_H 1

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include "Zp.h"
#include "PuntoW.h"
#include <boost/random.hpp>
#include <boost/multiprecision/cpp_int.hpp>

using namespace boost::multiprecision;
using namespace boost::random;

//DONE: cambiare il test di primalita' (miller rabin).
//DONE: modificare come viene usato il prodotto dei punti.
//DONE: rimuovere il calcolo degli ordini.
//DONE: interi a precisione arbitraria.
//DONE: modificare rand per lunghezza arbitraria.
//LIBRARY: sudo apt-get install libboost-all-dev (per la precisione arbitraria)

bool primo(cpp_int);
typedef independent_bits_engine<mt19937, 512, cpp_int> generator512_type;


// struttura per la gestione delle coppie per ottimizzare la conversione di elgamal,
// memorizzando una coppia valore-simbolo.
struct coppia
	{

	public:
	cpp_int valore;
	long simbolo;
	coppia(cpp_int valore, long simbolo): valore(valore), simbolo(simbolo)
		{}

	coppia()
		{}
	};



class ECC {

private:
		cpp_int max_primo; 
		unsigned min_periodo;
		generator512_type gen512;
		cpp_int p;

		Zn a;
		Zn b;

		cpp_int chiave_condivisa;
		cpp_int chiave_elgamal_privata;

		PuntoECC G1_elgamal;
		PuntoECC G2_elgamal;
		PuntoECC G1_elgamal_extern;			
		PuntoECC G2_elgamal_extern;

		PuntoECC G;

		bool parametri_impostati;


		/****************************************/
		void genera_chiave_privata(bool reimposta = false)
			{
			if(!parametri_impostati)
				return;

			if(chiave_condivisa != 0)
				if(!reimposta)
					return;
			do
				{
				chiave_condivisa = gen512()%max_primo;
				}
			while(chiave_condivisa*G==PuntoECC(a,b));
			
			return;
			}		


		/****************************************/
		// genera una mappa simboli creando un array contenente i multipli successivi 
		// del punto passato convertiti in interi associati.
		coppia* crea_mappa_simboli(PuntoECC G) const {

			coppia* mappa_simboli = new coppia[min_periodo];			
			mappa_simboli[0] = coppia(p,0);
			PuntoECC T = G;

			for(unsigned i=1; i<min_periodo; ++i)
				{
				mappa_simboli[i] = coppia(PuntoECC_to_cpp_int(T),i);
				T = T+G;
				}
			ordina_mappa(mappa_simboli,min_periodo);

			return mappa_simboli;	
		}

		/****************************************/
		void ordina_mappa(coppia* mappa_simboli, long length) const
			{
			cpp_int pivot;
			long l, r;
 			if(length<=1)
				return;
			pivot = mappa_simboli[0].valore;
			l = 1;
			r = length;
			while(l < r)
				if (mappa_simboli[l].valore < pivot) 
					l++;
				else {
					r--;
					swap(mappa_simboli[l], mappa_simboli[r]); 
				}
			l--;
			swap(mappa_simboli[0], mappa_simboli[l]);

			ordina_mappa(mappa_simboli, l);
			ordina_mappa(mappa_simboli+r, length-r);

/*			//TODO: ottimizzare con mergesort o altro
			for(long i=0; i<length; ++i)
				for(long j=length-1; j>i; --j)			
					if(mappa_simboli[j].valore<mappa_simboli[j-1].valore)
						swap(mappa_simboli[j],mappa_simboli[j-1]);
*/		
			return;
			}
		/******************************************/
		// decifra l'intero convertendolo in punto e cercando quale multiplo 
		// sono del punto base
		char decifra(cpp_int x) {
			PuntoECC T = cpp_int_to_PuntoECC(x);
				for(unsigned i = 0; i<min_periodo; ++i)
					if(i*G == T) 
						return (char)i;
		
			throw "impossibile decifrare, il punto non e' del tipo n*G.";

			}

		/******************************************/
		cpp_int genera_primo(){
			cpp_int p;
			do			
				{
				//TODO: controllare max_primo che sia maggiore di 10*min_periodo
				p = gen512()%(max_primo-10*min_periodo) + 10*min_periodo;//rand()%(max_primo-10*min_periodo) + 10*min_periodo;
				}
			while(!test_primo(p));
			return p;
			}

		/******************************************/
		void genera_ECC(){
			for(cpp_int i=0; i<50; ++i)
				{
				a= Zn(gen512(),p);
				b= Zn(gen512(),p);
				if(test_ECC())
					return;				
				}
			return;		
			}

		/******************************************/
		// testa se p e' primo confrontado 'p' con il suo piu' piccolo fattore 
		// primo che divide 'p'
		bool test_primo(cpp_int p) 
			{return primo(p);}

		/******************************************/
		bool test_ECC()	
			{return (4*a*a*a + 27*b*b != Zn(0,p));}

		/******************************************/
/*		// calcola il numero di punto della curva ellittica ricercando i punti
		// al variare di x 
		void calcola_numero_punti_ECC() 
			{
			Zn y = Zn(0,p);
			Zn y2 = Zn(0,p);
			cpp_int cont;

			// il punto all'infinito esiste sempre, quindi si conta
			cont = 1; 

			for(cpp_int i=0; i<p; ++i)
				{
				Zn x(i,p);
				y2 = x*x*x + a*x + b;

				// se y^2 == 0 allora esiste un y tale che (x,y) appartiene ad ECC 
				// e lo conto (solo y := 0, quindi solo un punto)				
				if(y2 == Zn(0,p))
					{
					++cont;
					}
				// altrimenti guardo se y2 e' un residuo quadratico
				else
 					{
					y = radice_quadrata_discreta(y2); // restituisce 0 se non e' un residuo quadratico
					// se e' un residuo quadratico, y != 0 e i punti (x,y) e (x,-y) appartengono alla curva 
					// e li aggiungo al conteggio
					if(y != Zn(0,p))
						if(y!=(-1*y)) 
							cont += 2;
						// questa parte solo per test. 
						else
							{
							++cont;
							}
					}
				}

			N = cont;

			return;
		}
*/
		/******************************************/
		// verifica che l'ordine di un punto sia almeno k
		bool ordine_almeno(PuntoECC G, cpp_int k) {

			PuntoECC T = G+G;
			for(unsigned i=1; i<k; ++i)
				{
				if(T==G)
					return false;
				T=T+G;
				}
							
			return true;
			}
/**/

		/******************************************/
		/******************************************/
		/******************************************/
		// genera la chiave privata e le due chiavi pubbliche per
		// la cifratura e decifratura Elgamal 
		void genera_chiave_elgamal(bool reimposta = false)
			{
			// se la chiave e' gia stata impostata (G1_elgamal != (inf,inf)) e
			// non viene richiesta una reimpostazione (reimposta == false) 
			// la funzione termina subito.
			//TODO portare fuori il test: non e' responsabilita' di questa funzione 
			if(G1_elgamal != 0*G1_elgamal)
				if(!reimposta)
					return;

			// assegna alla prima chiave publica un punto sulla curva 
			// che non sia il punto all'infinito.
			do 
				{
				G1_elgamal = genera_nuovo_punto();
				}
			while(G1_elgamal == PuntoECC(a,b));


			// genera la chiave privata e l'altra chiave pubblica,
			// assicurandosi che questa non sia il punto all'infinito.
			do
				{
				chiave_elgamal_privata = gen512()%p;
				G2_elgamal = chiave_elgamal_privata*G1_elgamal; 
				}
			while(G2_elgamal == PuntoECC(a,b) );

			return;
			}
	
		/******************************************/
		// genera un punto casuale sulla curva.
		PuntoECC genera_nuovo_punto() 
			{
			bool punto_trovato = false;
			PuntoECC res(a,b);	
			Zn x = Zn(gen512(),p);

			// ricomincia il ciclo finche' non viene trovato un punto valido.
			while(!punto_trovato)
				{
				// genera casualmente x e calcola il relativo y, se esiste.
				x = Zn(gen512(),p);
				Zn y = radice_quadrata_discreta(x*x*x + a*x + b);
			
				// controlla che il punto casuale generato appartenga alla curva
				// e che non sia il punto all'infinito.
				try
					{
					res = PuntoECC(x,y,a,b);
					if(res != PuntoECC(a,b))
						punto_trovato = true;
					}
				catch(const char* e)
					{}

				}
			return res;

			}

		/******************************************/
		// converte il punto condificato in un PuntoECC.
		//
		// (funzione specializzante) 
		PuntoECC cpp_int_to_PuntoECC(const cpp_int& l) const
			{return cpp_int_to_PuntoECC(l,p,a,b);}

		// (funzione generica)
		PuntoECC cpp_int_to_PuntoECC(const cpp_int& l, const cpp_int& p, const Zn& a, const Zn& b ) const
			{
			// calcola x del punto
			if(l==p)
				return PuntoECC(a,b);

			Zn x(l,p);
			if(l<0)
				x=Zn(-1*l,p);

			// calcola y del punto
			Zn y2 = (x*x*x) + (a*x) + b;
			Zn y = radice_quadrata_discreta(y2);

			if( y2 != Zn(0,p) && y==0)
				{
				throw "errore conversione cpp_int->PuntoECC";	
				}
			// crea il punto
			PuntoECC res(x, y, a, b);

			// corregge eventualmente l'ascissa del punto
			if( l<0 && res.get_y().get_v() < (p/2) )
				res = -1*res;
			else if( l>0 && res.get_y().get_v() > (p/2) )
				res = -1*res;

			return res;
			}

		/******************************************/
		// converte il punto in un intero corrispondente al valore di ascissa
		// positiva se l'ordinata è minore di 'p/2', negativa altrimenti. 
		cpp_int PuntoECC_to_cpp_int(const PuntoECC& P) const
			{
			if(P == cpp_int(0)*P)
				return p;

			cpp_int res = P.get_x().get_v();
			if(P.get_y().get_v() > (P.get_y().get_p()/2))
				res = -1*res;
		
			return res;	
			}
		/******************************************/
		// cerca il valore 'x' in un array 'mappa' lungo 'n' e ne
		// restituisce l'indice.
		long cerca_in_mappa(coppia* mappa, long length, cpp_int x) const
			{
			long primo=0;
			long ultimo=length-1;
			long medio;
			
			while(primo < ultimo)
				{
				medio = (ultimo+primo)/2;
				if(mappa[medio].valore == x)				
					return mappa[medio].simbolo;

				if(mappa[medio].valore < x)				
					primo = medio + 1;
				else
					ultimo = medio - 1;

				}
			medio = (ultimo+primo)/2;
			
			if(mappa[medio].valore == x)
				return mappa[medio].simbolo;
/*			//TODO: ottimizzare la ricerca
			for(unsigned i=0; i<length; ++i)
				if(x == mappa[i])
					return i;
*/
			return -1;
			}
		/******************************************/
		// converte il simbolo in un intero associato ad un punto a sua volta
		// associato al simbolo
		cpp_int cifra(char c) {

				// ottiene n convertendo c in scalare
				cpp_int n = (int)c;
			
				// converte l'intero in un punto e restituisce 
				// il valore codificato assoociato
				PuntoECC R = n*G;
				return PuntoECC_to_cpp_int(R);

				}
public:


		/******************************************/
		ECC():  max_primo(pow(cpp_int("10"),10)), min_periodo(128), p(2),
				a(Zn(0,2)), b(Zn(0,2)), G(PuntoECC(a,b)),
				parametri_impostati(false), 
				chiave_condivisa(0), G1_elgamal(PuntoECC(a,b)),
		 		G2_elgamal(PuntoECC(a,b)), G1_elgamal_extern(PuntoECC(a,b)),			
		 		G2_elgamal_extern(PuntoECC(a,b)), chiave_elgamal_privata(0)			

			{
			// sequenza per l'impostazione del seme iniziale "casuale"
			int* seeds = new int(time(NULL)); 
			boost::random::seed_seq ss(seeds, seeds+1); 
			gen512.seed(ss);

			// questo perche' il codice ASCII ha 128 elementi (in realta' 
			// min_periodo indica la cardinalita' dell'insieme dei simboli,
			// quindi il minimo periodo che deve avere il punto per mappare 
			// tutti i simboli su punti diversi) 			
			min_periodo = 128;
			}



		/******************************************/
		// decifra cercando ogni volta il punto associato 
		string decifra(string messaggio) {

			string result = "";
			
			if(!parametri_impostati)
				return result;
		
			// uso 'istringstream' come 'stream di input' per fare 
			// il parse del messaggio
			istringstream iss(messaggio);
			cpp_int x;

			iss >> x;
			while(iss)
				{
				// decifra il simbolo attuale
				result.push_back(decifra(x));

				iss >> x;
				}

			return result;
			}

		/******************************************/
		// decifra cercando i valori in una mappa generata da interi 
		// associati ai multipli di G.
		string decifra_con_cache(string messaggio) {

			if(!parametri_impostati)
				return string("");

			string result = "";
			cpp_int x;
			long c;
			istringstream iss(messaggio);	
			coppia* mappa_simboli = crea_mappa_simboli(G);
			iss >> x;
			while(iss)			
				{
				// cerca il valore nella mappa, se non c'è lancia un'eccezzione
				c = cerca_in_mappa(mappa_simboli, min_periodo, x);
				if(c<0)
					{
					throw " impossibile decifrare, indice carattere non trovato. ";
					}
				// se lo trova lo accoda al messaggio decifrato 
				result.push_back((char) c);

				iss >> x;
				}

			delete [] mappa_simboli;
			return result;
		}

		/******************************************/
		// verifica la validità dei parametri della classe, 
		// e, se vanno bene, permette l'uso delle funzioni di cifratura
		bool valida()
			{
			if(!test_primo(p))
				{
				cout << "non primo" << endl;
				return false;
				}

			if(!test_ECC())
				{
				cout << "ECC non valida" << endl;
				return false;
				}

			if(G == PuntoECC(a,b))
				{
				cout << "G e' il punto all'infinito" << endl;
				return false;
				}

			if(!ordine_almeno(G, min_periodo))
				{
				unsigned i=0;
				PuntoECC T =G;
				while(T!=PuntoECC(a,b))
					{
					i++;
					T=T+G;
					}
				cout << "ordine G:" << i << endl; 
/**/
				return false;
				}

			parametri_impostati = true;
			return true;

			} 

		/******************************************/
		// cifra il messaggio associando ad ogni simbolo un intero 
		// rappresentante il punto associato codificato
		string cifra(string messaggio) {

			if(!parametri_impostati)
				return string("");
		
			cpp_int length = messaggio.length();

			ostringstream oss;
			for(unsigned i=0; i<length; ++i)
				oss << cifra(messaggio[i]) << " ";

			return oss.str();
		}

		/******************************************/
		string cifra_con_cache(string messaggio) 
			{
			if(!parametri_impostati)
				return string("");
		
			unsigned length = messaggio.length();

			coppia* mappa_simboli = crea_mappa_simboli(G);		

			ostringstream oss;
			//per ogni simbolo recupero il valore da inviare 
			//direttamente dalla mappa dei simboli.
			for(unsigned i=0; i<length; ++i)
				oss << cerca_in_mappa(mappa_simboli, min_periodo,(unsigned)messaggio[i]) << " ";

			delete [] mappa_simboli;

			return oss.str();

			}
		/******************************************/
		// imposta le chiavi casualmente.
		void imposta_chiavi() 
			{
			parametri_impostati = false;			

			do
				{
				// genera un numero primo.
				p = genera_primo();
				if(!test_primo(p))
					continue;				
				
				// genera una curva ellittica e ne calcola 
				// il numero di punti
				genera_ECC();
				if(!test_ECC())
					continue;

				// cerca un punto e ne calcola l'ordine
				for(unsigned i=0; i<50; ++i)
					{

					G = genera_nuovo_punto();

					// verifica se i parametri solo validi e
					// se lo sono termina il ciclo.
					if(valida())
						{
						break;
						}
					}						
				}			
			while(!valida());

			return;

			}

		/******************************************/
		// imposta le chiavi a partire dai parametri di input (non imposta le chiavi di Elgamal)
		void imposta_chiavi(cpp_int p, cpp_int a, cpp_int b, cpp_int x, cpp_int y)
			{
			parametri_impostati = false;			
			this->p = p;
			this->a = Zn(a,p);
			this->b = Zn(b,p);

			try {
				this->G = PuntoECC(Zn(x,p),Zn(y,p),Zn(a,p),Zn(b,p));
				}
			catch(const char* e)
				{
				return;
				}

			// testa che p sia primo e che la curva non sia degenere.
			if(!(test_primo(p) && test_ECC()))
				return;

			// testa se i parametri inseriti sono validi
			valida();
			return;
			}

		/******************************************/
		// imposta le chiavi a partire dal tutti i parametri di input
		void imposta_chiavi(cpp_int p, cpp_int a, cpp_int b, cpp_int x, cpp_int y, PuntoECC c1, PuntoECC c2)
			{
			// imposta i primi valori
			imposta_chiavi(p,a,b,x,y);
		
			// se i primi valori non sono adeguati non permette di impostare le altre chiavi
			if(!parametri_impostati)
				return;
		
			//imposta le chiavi Elgamal esterne
			G1_elgamal_extern = c1;		
			G2_elgamal_extern = c2;		
		
			}
		/******************************************/
		// cifra il messaggio in input secondo il metodo Elgamal
		// per curve ellittiche crittografiche
		string cifra_elgamal(string messaggio) 
			{
			if(!parametri_impostati)
				return string();

			ostringstream oss(messaggio);

			// ciclo principale: cifra singolarmente i valori dei singoli
			// simboli del messaggio. 		
			for(unsigned i=0; i<messaggio.length(); ++i)
				{
				// calcola il punto associato al simbolo (cioe' quante volte 
				// bisogna moltiplicare il punto base)
				PuntoECC c = (cpp_int(messaggio[i]))*G1_elgamal_extern;
				// sceglie un valore a caso (tranne '1' e '2')
				// al di sotto dell'intervallo di Hasse ~[p-sqrt(p), p+sqrt(p)]
				cpp_int x = gen512()%p;//rand()%(p-2-pow(p,1/2)) + 2;
			
				// mette la maschera alle chiavi e aggiunge il punto associato 
				// al simbolo alla seconda 
				PuntoECC y1 = x*G1_elgamal_extern;
				PuntoECC y2 = c + x*G2_elgamal_extern;

				//converte i due punti e li aggiunge al messaggio crittato 
				cpp_int l;
				l = PuntoECC_to_cpp_int(y1);
				oss << l << " ";
				l = PuntoECC_to_cpp_int(y2);
				oss << l << " ";
				}

			//restituisce il messaggio crittato
			return oss.str();
			}

		/******************************************/
		// decifra il messaggio in input secondo il metodo Elgamal
		// per curve ellittiche crittografiche
		string decifra_elgamal(string messaggio) const
			{		
			if(!parametri_impostati)
				return string();

			istringstream iss(messaggio);
			string result;
		
			// crea una mappa simboli per velocizzare la decodifica:
			// per ogni simbolo s (0 < s < min_periodo) viene memorizzato
			// il punto codificato nella mappa.
			coppia* mappa_simboli = crea_mappa_simboli(G1_elgamal);

			PuntoECC G = G1_elgamal;
			PuntoECC T = G1_elgamal;

			// ciclo principale: per ogni coppia di interi ricevuti 
			// cerca il simbolo associato
			while(iss)
				{
				// legge le coppie di valori e, se non si puo' piu' leggere, termina
				cpp_int y1,y2;
				iss >> y1;
				iss >> y2;
				if(!iss)
					break;

				//trasforma i due interi nei punti associati sulla curva
				PuntoECC Py1 = cpp_int_to_PuntoECC(y1);
				PuntoECC Py2 = cpp_int_to_PuntoECC(y2);
				// rimuove la "maschera di Elgamal" 
				Py1 = (-1*chiave_elgamal_privata)*Py1 + Py2; 
				// converte il punto trovato in intero per cercarlo nella mappa
				cpp_int x = PuntoECC_to_cpp_int(Py1);	
				long i = cerca_in_mappa(mappa_simboli, min_periodo, x);
				// se non lo trova, lancia una eccezzione
				if(i < 0)
					{
					throw "errore di decifrazione elgamal";
					}
			
				// se lo trova lo inserisce nel messaggio decrittato 
				result.push_back((char)i);

				}

			// cancella la mappa creata
			delete[] mappa_simboli;
		
			// restituisce il risultato
			return result;
		
			}

		/****************************************/
		// genera chiave_condivisa e le chiavi Elgamal e ritorna una stringa
		// che corrisponde alla chiave pubblica nel formato
		// 
		// "p a b G key*G G1_elgamal G2_elgamal" 
		//  
		string scambio_DH_invia_chiave_pubblica() 
			{
			if(!parametri_impostati)
				{
				return string("..");
				}
			genera_chiave_privata();
			genera_chiave_elgamal();	
			
			ostringstream oss("");
			oss << p << " " << a.get_v() << " " << b.get_v() << " " << G << " "  << (chiave_condivisa*G) << " "
				<< G1_elgamal << " " << G2_elgamal << endl;

			return oss.str();
			}

		/****************************************/
		void stampa_stato()
			{
			cout <<
			"max_primo:"<< max_primo<<endl<<
			"min_periodo:"<<min_periodo<<endl<<
			"p:" << p << endl <<
			"a:" << a << endl <<
			"b:" << b << endl <<
			"G:" << G << endl <<
			"G1_elgamal:" << G1_elgamal << endl <<
			"G2_elgamal:" << G2_elgamal << endl <<
		 	"G1_elgamal_extern:" <<	G1_elgamal_extern << endl <<	
		 	"G2_elgamal_extern:" << G2_elgamal_extern << endl <<
			"parametri_impostati:" << (parametri_impostati?"true":"false") << endl;	
			
			}

		/****************************************/
		// imposta i parametri dell'oggetto a partire da una stringa contenente le informazioni 
		// inviate da un'altro oggetto nel formato:
		//
		//   p a b G key*G G1_elgamal_extern G2_elgamal_extern
		// 
		string scambio_DH_ricevi_chiave_pubblica(string chiave)
			{
			istringstream iss(chiave);

			cpp_int pp,aa,bb;
			iss >> pp;
			iss >> aa;
			iss >> bb;
			Zn a2(aa,pp);
			Zn b2(bb,pp);
			PuntoECC GG(a2,b2), TT(a2,b2), GE1(a2,b2), GE2(a2,b2);
			iss >> GG;
			iss >> TT;
			iss >> GE1;
			iss >> GE2;
		
			imposta_chiavi(pp, aa, bb, GG.get_x().get_v(), GG.get_y().get_v(), GE1, GE2);
			
			if(!parametri_impostati)
				{
//				cout << "Parametri non impostati" << endl;				
				return string("");	
				}
			string result = scambio_DH_invia_chiave_pubblica();
			
			//imposto G come il punto segreto condiviso tramite la funzione 
			TT = chiave_condivisa*TT;
			imposta_chiavi(pp, aa, bb, TT.get_x().get_v(), TT.get_y().get_v(),GE1,GE2);

			return result;
			}

};



/****************************************************/
// test primalita' di Miller-Rabin
bool primo(cpp_int p)
	{
	generator512_type gen512;
	
	if(p<0)
		p = -1*p;

    if(p<2)
        return false;
		
	if(p==2)
		return true;

    if ((p!=2) && (p%2)==0)
        return false;

    cpp_int d = p-1;
	cpp_int s = 1;
    while (d%2 == 0)
        {
		d /= 2;
		++s;
		}

    for (int i = 0; i < 1000; ++i)
		{
		cpp_int a = gen512()%(p-2) + 1;
		cpp_int r = 0;
		// esponente di a, inizialmente con r = 0.
		cpp_int esp = d;
		cpp_int res = (Zn(a,p)^esp).get_v();

	    while (r != s-1 && res != 1 && res != p-1)
		    {
	        res = (res*res)%p;
	        ++r;
		    }

	    if (res != p-1 && r>0)
			return false;
		}

	return true;
	}


#endif
