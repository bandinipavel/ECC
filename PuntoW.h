#ifndef GUARD_PuntoECC_H
#define GUARD_PuntoECC_H 1

#include <iostream>
#include <cstring>
#include <boost/multiprecision/cpp_int.hpp>
#include "Zp.h"
using namespace boost::multiprecision;

using namespace std;

class PuntoECC;
bool operator==(PuntoECC a, PuntoECC b);
PuntoECC operator+(PuntoECC a, PuntoECC b);
PuntoECC operator*(Zn k, PuntoECC a);

class PuntoECC {
	
	private: 
		//P = (x,y).
		Zn x;
		Zn y;
		//ECC: y^2 = x^3 + ax +b.
		Zn a;
		Zn b;
		bool infinito;
	public:
		
		PuntoECC(Zn x, Zn y, Zn a, Zn b): x(x), y(y), a(a), b(b), infinito(false)
			{
			if(y*y != ((x*x*x) + (a*x) + b)){
				throw "il punto non appartiene alla ECC";
				}
			} 

		PuntoECC(Zn a, Zn b):x(Zn(1,2)), y(Zn(1,2)), a(a), b(b), infinito(true)	
			{}

		Zn get_x() const
			{return x;}

		Zn get_y() const
			{return y;}

		Zn get_a() const
			{return a;}

		Zn get_b() const
			{return b;}

		bool is_infinito() const
			{return infinito;}

		cpp_int get_p() const
			{
			if(infinito)
				return 0;
			
			return a.get_p();
			}	

};

PuntoECC operator+(PuntoECC P, PuntoECC Q)
	{
	PuntoECC infinito = PuntoECC(P.get_a(),P.get_b());

	// caso: uno dei due è infinito 
	if(P==infinito)
		return Q;		
	if(Q==infinito)
		return P;

	if(P.get_p() != Q.get_p())
		throw "operazione tra punti in diversi Zn non gestita.";

	if(P.get_a() != Q.get_a() || P.get_b() != Q.get_b())
		throw "operazione tra punti in diverse ECC non gestita.";

	
	// caso entrambi non PuntoECC() (cioe' punto infinito).
	if(P.get_x() == Q.get_x())
		{
		if(P.get_y() != Q.get_y()) // caso (Q=-P): P+Q =infinito
			return infinito;
		else if(P.get_y() == Q.get_y() )// caso P == Q: 2P = res
			{
			if(Q.get_y()==0)
				return infinito;
	
			//ottengo la ECC e Zn
			Zn a_p = P.get_a();
			Zn b_p = P.get_b();		

			Zn x1 = P.get_x();
			Zn y1 = P.get_y();

			//xres = ((3*x1^2+a)/(2*y1))^2 - 2*x1;
			Zn m= ((3*(x1*x1) + a_p)/(2*y1));
			Zn xr = m*m - 2*x1;

			//yres = -y1 + (3*x1^2+a)*(x1-x3)/(2*y1)
			Zn yr = (y1) + (m*(xr-x1)) ;
			yr = (-1)*yr;
			return PuntoECC(xr,yr,a_p,b_p);
			}
		else
			throw "i punti non sono sulla stessa curva. (n.s.t.a.)";
		}
	else //caso: P!=Q e P!=-Q	
		{
			//ottengo la ECC e Zn
			Zn a_p = P.get_a();
			Zn b_p = P.get_b();		

			Zn x1 = P.get_x();
			Zn y1 = P.get_y();

			Zn x2 = Q.get_x();
			Zn y2 = Q.get_y();

			//xres = ((y2 - y1)/(x2 - x1))^2 - x1 - x2;
			Zn xr = (y2-y1)/(x2-x1);
			xr = xr*xr - x1 - x2;

			//yres = -y1 + (y2-y1)*(x1-x3)/(x2-x1)
			Zn yr = -1*y1  + ((y2-y1)/(x2-x1))*(x1-xr);
			
			return PuntoECC(xr,yr,a_p,b_p);
		}
	}



PuntoECC operator*(cpp_int k, PuntoECC P)
	{
	PuntoECC infinito = PuntoECC(P.get_a(), P.get_b());
	if(k == 0 || P == infinito)
		return infinito;

	// Prodotto con metodo di raddoppio della somma;
	PuntoECC result = infinito;

	// se k e' negativo, lo rende positivo e scambia il punto con il suo opposto
	// modificandogli il valore di 'y' in '-y' in Zn 	
	if(k<0)
		{
		k = -1*k;

		Zn new_y = Zn(-1*P.get_y().get_v(), P.get_y().get_p()); 	
		P = PuntoECC(P.get_x(), new_y, P.get_a(), P.get_b());
		}

	for(cpp_int i=k; i>0;)	
		{
		if(i%2 == 1)
			{
			result = result + P;
			--i;
			}

			P = P + P;
			i = i/2;			
		}

	return result;
	}

bool operator==(PuntoECC P, PuntoECC Q)
	{
	if(P.is_infinito() && Q.is_infinito() )
		return true;
	if(P.is_infinito() != Q.is_infinito() )
		return false;

	return( (P.get_x() == Q.get_x())  && P.get_y() == Q.get_y() );
	}

bool operator!=(PuntoECC P, PuntoECC Q)
	{return !(P==Q);}

ostream& operator<<(ostream& dest, PuntoECC P){
	if(!P.is_infinito())
		dest << "("<< P.get_x() << "," << P.get_y() << ")";
	else
		dest << "(inf,inf)";
	return dest;

}

istream& operator>>(istream& sorg, PuntoECC& P){
		char c;
		Zn x(0,1);
		Zn y(0,1);

		sorg >> c;
		//cout << "c="<< c<<endl;

		if(c!='(')		
			{
			sorg.setstate(ios::failbit);
			return sorg;
			
			}
		//spio il prossimo carattere per vedere se si tratta di un numero o di "inf"
		c = sorg.peek();
		//cout << "c="<< c<<endl;

		if(c!='i')
			{
			sorg >> x;
			//cout << "x="<< x<<endl;
			sorg >> c;
			//cout << "c="<< c<<endl;
			if(c!=',')		
				{
				sorg.setstate(ios::failbit);
				return sorg;			
				}
			sorg >> y;
			//cout << "y="<< y<<endl;
			sorg >> c;
			//cout << "c="<< c<<endl;
			if(c!=')')		
				{
				sorg.setstate(ios::failbit);
				return sorg;			
				}
			P = PuntoECC(x,y,P.get_a(),P.get_b());
			}
		else
			{
			char* s = new char[10];
			sorg.getline(s,9);
			if(strcmp(s,"inf,inf)")==0)
				P = PuntoECC(P.get_a(),P.get_b());	
			}

		return sorg;
}


#endif














